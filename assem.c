#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

#define COPANNN(A, NNN) (((((A) & 0xFU) << 12U) & 0xF000U) | ((NNN) & 0xFFFU))
#define COPAXKK(A, X, KK) (((((A) & 0xFU) << 12U) & 0xF000U) | ((((X) & 0xFU) << 8U) & 0xF00U) | ((KK) & 0xFFU))
#define COPAXYN(A, X, Y, N) (((((A) & 0xFU) << 12U) & 0xF000U) | ((((X) & 0xFU) << 8U) & 0xF00U) | ((((Y) & 0xFU) << 4U) & 0xF0U) | ((N) & 0xFU))

static int assem(const char *restrict cmd, const char *restrict p1, const char *restrict p2, const char *restrict p3, size_t np);
static int gn4b(const char *p);
static int gn8b(const char *p);
static int gn12b(const char *p);
static int gnumb(const char *p);
static int gvnum(const char *p);

static int
assem(const char *const restrict cmd, const char *const restrict p1, const char *const restrict p2, const char *const restrict p3, const size_t np)
{
	int v1, v2, v3, op;
	assert(cmd != NULL && p1 != NULL && p2 != NULL && p3 != NULL);
	op = -1;
	if (strcmp(cmd, SYMADD) == 0) {
		if (np != 2) {
			printe("Expected 2 parameters, got %zu.", np);
			return -1;
		}
		if (p1[0] == SYMV[0]) {
			if ((v1 = gvnum(p1)) < 0)
				return -1;
			if (p2[0] == SYMV[0]) {
				if ((v2 = gvnum(p2)) < 0)
					return -1;
				op = COPAXYN(0x8U, v1, v2, 0x4U);
			} else {
				if ((v2 = gn8b(p2)) < 0)
					return -1;
				op = COPAXKK(0x7U, v1, v2);
			}
		} else if (strcmp(p1, SYMI) == 0) {
			if ((v2 = gvnum(p2)) < 0)
				return -1;
			op = COPAXKK(0xFU, v2, 0x1EU);
		} else {
			printe("Invalid first parameter %s found.", p1);
			return -1;
		}
	} else if (strcmp(cmd, SYMAND) == 0) {
		if (np != 2) {
			printe("Expected 2 parameters, got %zu.", np);
			return -1;
		}
		if ((v1 = gvnum(p1)) < 0 || (v2 = gvnum(p2)) < 0)
			return -1;
		op = COPAXYN(0x8U, v1, v2, 0x2U);
	} else if (strcmp(cmd, SYMCALL) == 0) {
		if (np != 1) {
			printe("Expected 1 parameter, got %zu.", np);
			return -1;
		}
		if ((v1 = gn12b(p1)) < 0)
			return -1;
		op = COPANNN(0x2U, v1);
	} else if (strcmp(cmd, SYMCLS) == 0) {
		if (np != 0) {
			printe("Expected no parameters, got %zu.", np);
			return -1;
		}
		op = 0x00E0U;
	} else if (strcmp(cmd, SYMDRW) == 0) {
		if (np != 3) {
			printe("Expected 3 parameters, got %zu.", np);
			return -1;
		}
		if ((v1 = gvnum(p1)) < 0 || (v2 = gvnum(p2)) < 0 || (v3 = gn4b(p3)) < 0)
			return -1;
		op = COPAXYN(0xDU, v1, v2, v3);
	} else if (strcmp(cmd, SYMJP) == 0) {
		if (np < 1 || np > 2) {
			printe("Expected 1 or 2 parameters, got %zu.", np);
			return -1;
		}
		if (p1[0] == SYMV[0]) {
			if ((v2 = gn12b(p2)) < 0)
				return -1;
			op = COPANNN(0xBU, v2);
		} else {
			if ((v1 = gn12b(p1)) < 0)
				return -1;
			op = COPANNN(0x1U, v1);
		}
	} else if (strcmp(cmd, SYMLD) == 0) {
		if (np != 2) {
			printe("Expected 2 parameters, got %zu.", np);
			return -1;
		}
		if (p1[0] == SYMV[0]) {
			if ((v1 = gvnum(p1)) < 0)
				return -1;
			if (p2[0] == SYMV[0]) {
				if ((v2 = gvnum(p2)) < 0)
					return -1;
				op = COPAXYN(0x8U, v1, v2, 0x0U);
			} else if (strcmp(p2, SYMDT) == 0) {
				op = COPAXKK(0xFU, v1, 0x07U);
			} else if (strcmp(p2, SYMKEY) == 0) {
				op = COPAXKK(0xFU, v1, 0x0AU);
			} else if (strcmp(p2, SYMIM) == 0) {
				op = COPAXKK(0xFU, v1, 0x65U);
			} else {
				if ((v2 = gn8b(p2)) < 0)
					return -1;
				op = COPAXKK(0x6U, v1, v2);
			}
		} else if (strcmp(p1, SYMI) == 0) {
			if ((v2 = gn12b(p2)) < 0)
				return -1;
			op = COPANNN(0xAU, v2);
		} else if (strcmp(p1, SYMDT) == 0) {
			if ((v2 = gvnum(p2)) < 0)
				return -1;
			op = COPAXKK(0xFU, v2, 0x15U);
		} else if (strcmp(p1, SYMST) == 0) {
			if ((v2 = gvnum(p2)) < 0)
				return -1;
			op = COPAXKK(0xFU, v2, 0x18U);
		} else if (strcmp(p1, SYMFNT) == 0) {
			if ((v2 = gvnum(p2)) < 0)
				return -1;
			op = COPAXKK(0xFU, v2, 0x29U);
		} else if (strcmp(p1, SYMBCD) == 0) {
			if ((v2 = gvnum(p2)) < 0)
				return -1;
			op = COPAXKK(0xFU, v2, 0x33U);
		} else if (strcmp(p1, SYMIM) == 0) {
			if ((v2 = gvnum(p2)) < 0)
				return -1;
			op = COPAXKK(0xFU, v2, 0x55U);
		} else {
			printe("Invalid first parameter %s found.", p1);
			return -1;
		}
	} else if (strcmp(cmd, SYMOR) == 0) {
		if (np != 2) {
			printe("Expected 2 parameters, got %zu.", np);
			return -1;
		}
		if ((v1 = gvnum(p1)) < 0 || (v2 = gvnum(p2)) < 0)
			return -1;
		op = COPAXYN(0x8U, v1, v2, 0x1U);
	} else if (strcmp(cmd, SYMRET) == 0) {
		if (np != 0) {
			printe("Expected no parameters, got %zu.", np);
			return -1;
		}
		op = 0x00EEU;
	} else if (strcmp(cmd, SYMRND) == 0) {
		if (np != 2) {
			printe("Expected 2 parameters, got %zu.", np);
			return -1;
		}
		if ((v1 = gvnum(p1)) < 0 || (v2 = gn8b(p2)) < 0)
			return -1;
		op = COPAXKK(0xCU, v1, v2);
	} else if (strcmp(cmd, SYMSE) == 0) {
		if (np != 2) {
			printe("Expected 2 parameters, got %zu.", np);
			return -1;
		}
		if ((v1 = gvnum(p1)) < 0)
			return -1;
		if (p2[0] == SYMV[0]) {
			if ((v2 = gvnum(p2)) < 0)
				return -1;
			op = COPAXYN(0x5U, v1, v2, 0x0U);
		} else {
			if ((v2 = gn8b(p2)) < 0)
				return -1;
			op = COPAXKK(0x3U, v1, v2);
		}
	} else if (strcmp(cmd, SYMSHL) == 0) {
		if (np != 1) {
			printe("Expected 1 parameter, got %zu.", np);
			return -1;
		}
		if ((v1 = gvnum(p1)) < 0)
			return -1;
		op = COPAXKK(0x8U, v1, 0x0EU);
	} else if (strcmp(cmd, SYMSHR) == 0) {
		if (np != 1) {
			printe("Expected 1 parameter, got %zu.", np);
			return -1;
		}
		if ((v1 = gvnum(p1)) < 0)
			return -1;
		op = COPAXKK(0x8U, v1, 0x06U);
	} else if (strcmp(cmd, SYMSKNP) == 0) {
		if (np != 1) {
			printe("Expected 1 parameter, got %zu.", np);
			return -1;
		}
		if ((v1 = gvnum(p1)) < 0)
			return -1;
		op = COPAXKK(0xEU, v1, 0xA1U);
	} else if (strcmp(cmd, SYMSKP) == 0) {
		if (np != 1) {
			printe("Expected 1 parameter, got %zu.", np);
			return -1;
		}
		if ((v1 = gvnum(p1)) < 0)
			return -1;
		op = COPAXKK(0xEU, v1, 0x9EU);
	} else if (strcmp(cmd, SYMSNE) == 0) {
		if (np != 2) {
			printe("Expected 2 parameters, got %zu.", np);
			return -1;
		}
		if ((v1 = gvnum(p1)) < 0)
			return -1;
		if (p2[0] == SYMV[0]) {
			if ((v2 = gvnum(p2)) < 0)
				return -1;
			op = COPAXYN(0x9U, v1, v2, 0x0U);
		} else {
			if ((v2 = gn8b(p2)) < 0)
				return -1;
			op = COPAXKK(0x4U, v1, v2);
		}
	} else if (strcmp(cmd, SYMSUB) == 0) {
		if (np != 2) {
			printe("Expected 2 parameters, got %zu.", np);
			return -1;
		}
		if ((v1 = gvnum(p1)) < 0 || (v2 = gvnum(p2)) < 0)
			return -1;
		op = COPAXYN(0x8U, v1, v2, 0x5U);
	} else if (strcmp(cmd, SYMSUBN) == 0) {
		if (np != 2) {
			printe("Expected 2 parameters, got %zu.", np);
			return -1;
		}
		if ((v1 = gvnum(p1)) < 0 || (v2 = gvnum(p2)) < 0)
			return -1;
		op = COPAXYN(0x8U, v1, v2, 0x7U);
	} else if (strcmp(cmd, SYMSYS) == 0) {
		if (np != 1) {
			printe("Expected 1 parameter, got %zu.", np);
			return -1;
		}
		if ((v1 = gn12b(p1)) < 0)
			return -1;
		op = COPANNN(0x0U, v1);
	} else if (strcmp(cmd, SYMXOR) == 0) {
		if (np != 2) {
			printe("Expected 2 parameters, got %zu.", np);
			return -1;
		}
		if ((v1 = gvnum(p1)) < 0 || (v2 = gvnum(p2)) < 0)
			return -1;
		op = COPAXYN(0x8U, v1, v2, 0x3U);
	} else {
		printe("Invalid command %s found.", cmd);
		return -1;
	}
	assert(op != -1);
	return op;
}

static int
gn4b(const char *const p) {
	int v;
	assert(p != NULL);
	if ((v = gnumb(p)) < 0)
		return -1;
	if (v > 0xF) {
		printe("Invalid number %s, must be at most 4-bits.", p);
		return -1;
	}
	return v & 0xFU;
}

static int
gn8b(const char *const p) {
	int v;
	assert(p != NULL);
	if ((v = gnumb(p)) < 0)
		return -1;
	if (v > 0xFF) {
		printe("Invalid number %s, must be at most 8-bits.", p);
		return -1;
	}
	return v & 0xFFU;
}

static int
gn12b(const char *const p)
{
	int v;
	assert(p != NULL);
	if ((v = gnumb(p)) < 0)
		return -1;
	if (v > 0xFFF) {
		printe("Invalid number %s, must be at most 12-bits.", p);
		return -1;
	}
	return v & 0xFFFU;
}

static int
gnumb(const char *const p)
{
	long v;
	char *ep;
	assert(p != NULL);
	if ((v = strtol(p, &ep, 0)) == LONG_MIN || v == LONG_MAX || !(*p != '\0' && *ep == '\0')) {
		printe("Failed to convert parameter %s to a valid number.", p);
		return -1;
	}
	if (v < 0) {
		printe("Invalid parameter %s, must be a positive integer.", p);
		return -1;
	}
	return v & 0xFFFF;
}

static int
gvnum(const char *const p)
{
	assert(p != NULL);
	if (p[0] != SYMV[0] || p[2] != '\0') {
		printe("Invalid register %s provided.", p);
		return -1;
	}
	if (p[1] >= '0' && p[1] <= '9')
		return p[1] - '0';
	if (p[1] >= 'a' && p[1] <= 'f')
		return 10 + (p[1] - 'a');
	if (p[1] >= 'A' && p[1] <= 'F')
		return 10 + (p[1] - 'A');
	return -1;
}

int
main(int argc, char *argv[])
{
	FILE *f;
	size_t lc, rl;
	int ct, rc, op;
	unsigned char rom[ROMSZMX];
	char ln[128], cmd[8], p1[8], p2[8], p3[8];
	if (argc != 3) {
		printf("Usage: ./asm.x [source] [output]\n\n");
		printf("\tsource - The source file to assemble.\n");
		printf("\toutput - The output file to write to.\n");
		return EXIT_SUCCESS;
	}
	if ((f = fopen(argv[1], "r")) == NULL) {
		printe("Unable to open file %s.", argv[1]);
		return EXIT_FAILURE;
	}
	rc = EXIT_SUCCESS;
	rl = rom[0] = 0;
	for (lc = 1; fgets(ln, sizeof(ln), f) != NULL; ++lc) {
		cmd[0] = p1[0] = p2[0] = p3[0] = '\0';
		ct = sscanf(ln, "%8[a-zA-Z] %8[][a-zA-Z0-9] , %8[][a-zA-Z0-9] , %8[][a-zA-Z0-9]", cmd, p1, p2, p3);
		if (ct == 0)
			continue;
		--ct;
		if ((op = assem(cmd, p1, p2, p3, ct)) < 0) {
			printe("Failed to assemble line %zu.", lc);
			rc = EXIT_FAILURE;
			goto end;
		}
		rom[rl++] = ((op & 0xFF00U) >> 8U) & 0xFFU;
		rom[rl++] = op & 0xFFU;
	}
	if (ferror(f) != 0) {
		printe("Failed to read from file %s.", argv[1]);
		rc = EXIT_FAILURE;
	}
	fclose(f);
	if ((f = fopen(argv[2], "wb")) == NULL) {
		printe("Unable to open file %s.", argv[2]);
		return EXIT_FAILURE;
	}
	if (fwrite(rom, sizeof(unsigned char), rl, f) < rl && ferror(f) != 0) {
		printe("Unable to write to file %s.", argv[2]);
		rc = EXIT_FAILURE;
	}
end:
	fclose(f);
	return rc;
}
