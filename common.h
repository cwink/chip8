#ifndef COMMON_H
#define COMMON_H

#define ROMSZMX 3582

#define SYMADD  "ADD"
#define SYMAND  "AND"
#define SYMBCD  "B"
#define SYMCALL "CALL"
#define SYMCLS  "CLS"
#define SYMDRW  "DRW"
#define SYMDT   "DT"
#define SYMFNT  "F"
#define SYMI    "I"
#define SYMIM   "[I]"
#define SYMJP   "JP"
#define SYMKEY  "K"
#define SYMLD   "LD"
#define SYMOR   "OR"
#define SYMRET  "RET"
#define SYMRND  "RND"
#define SYMSE   "SE"
#define SYMSHL  "SHL"
#define SYMSHR  "SHR"
#define SYMSKNP "SKNP"
#define SYMSKP  "SKP"
#define SYMSNE  "SNE"
#define SYMST   "ST"
#define SYMSUB  "SUB"
#define SYMSUBN "SUBN"
#define SYMSYS  "SYS"
#define SYMV    "V"
#define SYMXOR  "XOR"

#define OPA(OP)   ((((OP) & 0xF000U) >> 12U) & 0xFU)
#define OPKK(OP)  ((OP) & 0xFFU)
#define OPN(OP)   ((OP) & 0xFU)
#define OPNNN(OP) ((OP) & 0xFFFU)
#define OPX(OP)   ((((OP) & 0xF00U) >> 8U) & 0xFU)
#define OPY(OP)   ((((OP) & 0xF0U) >> 4U) & 0xFU)

#ifndef UNUSED
#	ifdef __GNUC__
#		define UNUSED __attribute__((unused))
#	else
#		define UNUSED
#	endif
#endif /* UNUSED */

void printe(const char *restrict fmt, ...);

#endif /* COMMON_H */
