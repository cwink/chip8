#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <SDL2/SDL.h>

#include "common.h"

#define CPUHZ  (1.0 / 700.0)
#define DTMCAP 1.0
#define GFXWD  64
#define GFXHT  32
#define OFFCOL 0x282828FFU
#define ONCOL  0xEBDBB2FFU
#define PCST   512
#define SQWVLEN 1024
#define SQWVVOL 10
#define TIMHZ  (1.0 / 60.0)
#define WINSCL 10
#define WINTIT "Chip8 Emu"

static void          acallb(void *udata, Uint8 *strm, int len);
static int           edraw(unsigned short vx, unsigned short vy, unsigned short n);
static int           eload(const char *fname);
static void          ereset(void);
static int           estep(void);
static int           gclear(void);
static int           ginit(void);
static int           gkwp(unsigned char key);
static int           gpixg(unsigned short x, unsigned short y);
static int           gpixs(unsigned short x, unsigned short y, int on);
static int           gpoll(void);
static int           grend(void);
static void          gterm(void);
static unsigned char gwaitk(void);

static SDL_Window        *win;
static SDL_Renderer      *ren;
static SDL_Texture       *tex;
static SDL_AudioDeviceID  dev;
static unsigned short ir, pc, sp, sta[16];
static unsigned char  dt, rom[ROMSZMX + PCST], st, vr[16];

static unsigned char fnts[80] = {
	0xF0, 0x90, 0x90, 0x90, 0xF0, 0x20, 0x60, 0x20, 0x20, 0x70,
	0xF0, 0x10, 0xF0, 0x80, 0xF0, 0xF0, 0x10, 0xF0, 0x10, 0xF0,
	0x90, 0x90, 0xF0, 0x10, 0x10, 0xF0, 0x80, 0xF0, 0x10, 0xF0,
	0xF0, 0x80, 0xF0, 0x90, 0xF0, 0xF0, 0x10, 0x20, 0x40, 0x40,
	0xF0, 0x90, 0xF0, 0x90, 0xF0, 0xF0, 0x90, 0xF0, 0x10, 0xF0,
	0xF0, 0x90, 0xF0, 0x90, 0x90, 0xE0, 0x90, 0xE0, 0x90, 0xE0,
	0xF0, 0x80, 0x80, 0x80, 0xF0, 0xE0, 0x90, 0x90, 0x90, 0xE0,
	0xF0, 0x80, 0xF0, 0x80, 0xF0, 0xF0, 0x80, 0xF0, 0x80, 0x80
};

static void
acallb(UNUSED void *udata, Uint8 *strm, int len)
{
	Sint8 *au, *ed;
	char v;
	ed = (Sint8 *)strm + len;
	v = -SQWVVOL;
	for (au = (Sint8 *)strm; au != ed; ++au) {
		if ((au - (Sint8 *)strm) % (SQWVLEN / 2) == 0)
			v *= -1;
		*au = v;
	}
}

static int
edraw(const unsigned short vx, const unsigned short vy, const unsigned short n)
{
	int rc;
	unsigned short i, ii, x, xx, y, yy;
	i = ir;
	vr[0xF] = 0x0;
	for (y = 0; y < n; ++y) {
		ii = rom[i];
		yy = (vy + y) % GFXHT;
		for (x = 0; x < 8; ++x) {
			xx = (vx + x) % GFXWD;
			if ((ii & 0x80U) == 0x80U) {
				if ((rc = gpixg(xx, yy)) < 0)
					return -1;
				if (rc) {
					if ((rc = gpixs(xx, yy, 0)) < 0)
						return -1;
					vr[0xF] = 0x1;
				} else {
					if ((rc = gpixs(xx, yy, 1)) < 0)
						return -1;
				}
			}
			ii = (ii & 0x7FU) << 1U;
		}
		++i;
	}
	return 0;
}

static int
eload(const char *const fname)
{
	FILE *f;
	assert(fname != NULL);
	if ((f = fopen(fname, "rb")) == NULL) {
		printe("Unable to open rom %s.", fname);
		return -1;
	}
	memset(rom, 0, sizeof(rom));
	memcpy(rom, fnts, sizeof(fnts));
	if (fread(rom + PCST, sizeof(unsigned char), ROMSZMX, f) < 1 && ferror(f) != 0) {
		printe("Failed to read from rom %s.", fname);
		fclose(f);
		return -1;
	}
	fclose(f);
	return 0;
}

static void
ereset(void)
{
	memset(sta, 0, sizeof(sta));
	memset(vr, 0, sizeof(vr));
	pc = PCST;
	ir = sp = dt = st = 0;
}

static int
estep(void)
{
	unsigned short t;
	const unsigned short op = (rom[pc] << 8U & 0xFF00U) | rom[pc + 1];
	switch (OPA(op)) {
	case 0x0:
		switch (OPKK(op)) {
		case 0xE0:
			if (gclear() < 0)
				goto failure;
			pc += 2;
			break;
		case 0xEE:
			pc = sta[sp--];
			pc += 2;
			break;
		default:
			printf("%-5s %u\n", SYMSYS, OPNNN(op));
			pc += 2;
			break;
		}
		break;
	case 0x1:
		pc = OPNNN(op);
		break;
	case 0x2:
		sta[++sp] = pc;
		pc = OPNNN(op);
		break;
	case 0x3:
		if (vr[OPX(op)] == OPKK(op))
			pc += 2;
		pc += 2;
		break;
	case 0x4:
		if (vr[OPX(op)] != OPKK(op))
			pc += 2;
		pc += 2;
		break;
	case 0x5:
		if (OPN(op) != 0x0)
			goto failure;
		if (vr[OPX(op)] == vr[OPY(op)])
			pc += 2;
		pc += 2;
		break;
	case 0x6:
		vr[OPX(op)] = OPKK(op);
		pc += 2;
		break;
	case 0x7:
		vr[OPX(op)] += OPKK(op);
		pc += 2;
		break;
	case 0x8:
		switch (OPN(op)) {
		case 0x0:
			vr[OPX(op)] = vr[OPY(op)];
			pc += 2;
			break;
		case 0x1:
			vr[OPX(op)] |= vr[OPY(op)];
			pc += 2;
			break;
		case 0x2:
			vr[OPX(op)] &= vr[OPY(op)];
			pc += 2;
			break;
		case 0x3:
			vr[OPX(op)] ^= vr[OPY(op)];
			pc += 2;
			break;
		case 0x4:
			t = vr[OPX(op)] + vr[OPY(op)];
			vr[0xF] = t > 0xFFU;
			vr[OPX(op)] = t & 0xFFU;
			pc += 2;
			break;
		case 0x5:
			vr[0xF] = vr[OPX(op)] > vr[OPY(op)];
			vr[OPX(op)] -= vr[OPY(op)];
			pc += 2;
			break;
		case 0x6:
			vr[0xF] = (vr[OPX(op)] & 0x1) == 0x1;
			vr[OPX(op)] >>= 1;
			pc += 2;
			break;
		case 0x7:
			vr[0xF] = vr[OPY(op)] > vr[OPX(op)];
			vr[OPX(op)] = vr[OPY(op)] - vr[OPX(op)];
			pc += 2;
			break;
		case 0xE:
			vr[0xF] = (vr[OPX(op)] & 0x80) == 0x80;
			vr[OPX(op)] <<= 1;
			pc += 2;
			break;
		default:
			goto failure;
		}
		break;
	case 0x9:
		if (OPN(op) != 0x0)
			goto failure;
		if (vr[OPX(op)] != vr[OPY(op)])
			pc += 2;
		pc += 2;
		break;
	case 0xA:
		ir = OPNNN(op);
		pc += 2;
		break;
	case 0xB:
		pc = vr[0x0] + OPNNN(op);
		break;
	case 0xC:
		vr[OPX(op)] = (unsigned char)(rand() % 256U) & OPKK(op);
		pc += 2;
		break;
	case 0xD:
		if (edraw(vr[OPX(op)], vr[OPY(op)], OPN(op)) < 0)
			goto failure;
		pc += 2;
		break;
	case 0xE:
		switch (OPKK(op)) {
		case 0x9E:
			if (gkwp(vr[OPX(op)]))
				pc += 2;
			pc += 2;
			break;
		case 0xA1:
			if (!gkwp(vr[OPX(op)]))
				pc += 2;
			pc += 2;
			break;
		default:
			goto failure;
		}
		break;
	case 0xF:
		switch (OPKK(op)) {
		case 0x07:
			vr[OPX(op)] = dt;
			pc += 2;
			break;
		case 0x0A:
			vr[OPX(op)] = gwaitk();
			pc += 2;
			break;
		case 0x15:
			dt = vr[OPX(op)];
			pc += 2;
			break;
		case 0x18:
			st = vr[OPX(op)];
			pc += 2;
			break;
		case 0x1E:
			ir += vr[OPX(op)];
			pc += 2;
			break;
		case 0x29:
			ir = vr[OPX(op)] * 5;
			pc += 2;
			break;
		case 0x33:
			rom[ir] = vr[OPX(op)] / 100;
			rom[ir + 1] = (vr[OPX(op)] / 10) % 10;
			rom[ir + 2] = (vr[OPX(op)] % 100) % 10;
			pc += 2;
			break;
		case 0x55:
			for (t = 0; t <= OPX(op); ++t)
				rom[ir + t] = vr[t];
			pc += 2;
			break;
		case 0x65:
			for (t = 0; t <= OPX(op); ++t)
				vr[t] = rom[ir + t];
			pc += 2;
			break;
		default:
			goto failure;
		}
		break;
	}
	return 0;
failure:
	printe("Invalid instruction 0x%4.4x encountered.", op);
	return -1;
}

static int
ginit(void)
{
	SDL_AudioSpec hv, wt;
	win = NULL;
	ren = NULL;
	tex = NULL;
	dev = 0;
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		printe("%s.", SDL_GetError());
		return -1;
	}
	if ((win = SDL_CreateWindow(WINTIT, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, GFXWD * WINSCL, GFXHT * WINSCL, 0)) == NULL) {
		printe("%s.", SDL_GetError());
		return -1;
	}
	if ((ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC)) == NULL) {
		printe("%s.", SDL_GetError());
		return -1;
	}
	if (SDL_RenderSetLogicalSize(ren, GFXWD, GFXHT) < 0) { 
		printe("%s.", SDL_GetError());
		return -1;
	}
	if ((tex = SDL_CreateTexture(ren, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, GFXWD, GFXHT)) == NULL) {
		printe("%s.", SDL_GetError());
		return -1;
	}
	wt.freq = 44100;
	wt.format = AUDIO_S8;
	wt.channels = 1;
	wt.samples = 1024;
	wt.callback = acallb;
	if ((dev = SDL_OpenAudioDevice(NULL, 0, &wt, &hv, 0)) == 0) {
		printe("%s.", SDL_GetError());
		return -1;
	}
	return 0;
}

static int
gkwp(const unsigned char key)
{
	switch (key) {
	case 0x0:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_0];
	case 0x1:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_1];
	case 0x2:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_2];
	case 0x3:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_3];
	case 0x4:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_4];
	case 0x5:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_5];
	case 0x6:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_6];
	case 0x7:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_7];
	case 0x8:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_8];
	case 0x9:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_9];
	case 0xA:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_A];
	case 0xB:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_B];
	case 0xC:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_C];
	case 0xD:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_D];
	case 0xE:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_E];
	case 0xF:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_F];
	}
	assert(0);
	return 0;
}

static int
gclear(void)
{
	unsigned int i;
	int pt;
	void *px;
	if (SDL_LockTexture(tex, NULL, &px, &pt) < 0) {
		printe("%s.", SDL_GetError());
		return -1;
	}
	for (i = 0; i < GFXHT * GFXWD; ++i)
		((Uint32 *)px)[i] = OFFCOL;
	SDL_UnlockTexture(tex);
	return 0;
}

static int
gpixg(const unsigned short x, const unsigned short y)
{
	int o, pt;
	void *px;
	if (SDL_LockTexture(tex, NULL, &px, &pt) < 0) {
		printe("%s.", SDL_GetError());
		return -1;
	}
	o = ((Uint32 *)px)[(y * GFXWD) + x] == ONCOL;
	SDL_UnlockTexture(tex);
	return o;
}

static int
gpixs(const unsigned short x, const unsigned short y, const int on)
{
	int pt;
	void *px;
	if (SDL_LockTexture(tex, NULL, &px, &pt) < 0) {
		printe("%s.", SDL_GetError());
		return -1;
	}
	((Uint32 *)px)[(y * GFXWD) + x] = on ? ONCOL : OFFCOL;
	SDL_UnlockTexture(tex);
	return 0;
}

static int
gpoll(void)
{
	SDL_Event ev;
	while (SDL_PollEvent(&ev)) {
		if (ev.type == SDL_WINDOWEVENT && ev.window.event == SDL_WINDOWEVENT_CLOSE)
			return 1;
	}
	return 0;
}

static int
grend(void)
{
	if (SDL_RenderCopy(ren, tex, NULL, NULL) < 0) {
		printe("%s.", SDL_GetError());
		return -1;
	}
	SDL_RenderPresent(ren);
	SDL_PauseAudioDevice(dev, st > 0 ? 0 : 1);
	SDL_Delay(10);
	return 0;
}

static void
gterm(void)
{
	if (dev != 0)
		SDL_CloseAudioDevice(dev);
	if (tex != NULL)
		SDL_DestroyTexture(tex);
	if (ren != NULL)
		SDL_DestroyRenderer(ren);
	if (win != NULL)
		SDL_DestroyWindow(win);
	SDL_Quit();
}

static unsigned char
gwaitk(void)
{
	SDL_Event ev;
	unsigned char key;
	while (1) {
		while (SDL_PollEvent(&ev)) {
			if (ev.key.type == SDL_KEYDOWN && ev.key.state == SDL_PRESSED) {
				switch (ev.key.keysym.scancode) {
				case SDL_SCANCODE_0:
					key = 0x0;
					break;
				case SDL_SCANCODE_1:
					key = 0x1;
					break;
				case SDL_SCANCODE_2:
					key = 0x2;
					break;
				case SDL_SCANCODE_3:
					key = 0x3;
					break;
				case SDL_SCANCODE_4:
					key = 0x4;
					break;
				case SDL_SCANCODE_5:
					key = 0x5;
					break;
				case SDL_SCANCODE_6:
					key = 0x6;
					break;
				case SDL_SCANCODE_7:
					key = 0x7;
					break;
				case SDL_SCANCODE_8:
					key = 0x8;
					break;
				case SDL_SCANCODE_9:
					key = 0x9;
					break;
				case SDL_SCANCODE_A:
					key = 0xA;
					break;
				case SDL_SCANCODE_B:
					key = 0xB;
					break;
				case SDL_SCANCODE_C:
					key = 0xC;
					break;
				case SDL_SCANCODE_D:
					key = 0xD;
					break;
				case SDL_SCANCODE_E:
					key = 0xE;
					break;
				case SDL_SCANCODE_F:
					key = 0xF;
					break;
				default:
					continue;
				}
				goto finish;
			}
		}
	}
finish:
	return key;
}

int
main(int argc, char *argv[])
{
	double         acc, act, dtm;
	Uint32         ctm, ltm;
	int ec, rc;
	if (argc != 2) {
		printf("Usage: ./emu.x [rom]\n\n");
		printf("\trom - The rom to emulate.\n");
		return EXIT_SUCCESS;
	}
	if (eload(argv[1]) < 0)
		return EXIT_FAILURE;
	ereset();
	srand(time(NULL));
	if (ginit() < 0) {
		rc = EXIT_FAILURE;
		goto end;
	}
	if (gclear() < 0) {
		rc = EXIT_FAILURE;
		goto end;
	}
	rc = EXIT_SUCCESS;
	ltm = SDL_GetTicks();
	acc = act = 0.0;
	while (1) {
		ctm = SDL_GetTicks();
		dtm = ((double)ctm / 1000.0) - ((double)ltm / 1000.0);
		if (dtm > DTMCAP)
			dtm = 1.0;
		ltm = ctm;
		acc += dtm;
		act += dtm;
		if ((ec = gpoll()) < 0) {
			rc = EXIT_FAILURE;
			goto end;
		}
		if (ec > 0)
			break;
		while (acc >= CPUHZ) {
			if (estep() < 0) {
				rc = EXIT_FAILURE;
				goto end;
			}
			acc -= CPUHZ;
		}
		while (act >= TIMHZ) {
			if (dt > 0)
				--dt;
			if (st > 0)
				--st;
			act -= TIMHZ;
		}
		if (grend() < 0) {
			rc = EXIT_FAILURE;
			goto end;
		}
	}
end:
	gterm();
	return rc;
}
