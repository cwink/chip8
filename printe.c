#include <assert.h>
#include <stdarg.h>
#include <stdio.h>

#include "common.h"

void
printe(const char *const restrict fmt, ...)
{
	va_list args;
	assert(fmt != NULL);
	fprintf(stderr, "ERROR: ");
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "\n");
	va_end(args);
}
