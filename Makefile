.POSIX:
.SUFFIXES:

CC      = gcc
CFLAGS  = -std=c99 -pedantic-errors -Wall -Wextra -O3 -DNDEBUG -D_POSIX_C_SOURCE=200809L
#CFLAGS  = -std=c99 -pedantic-errors -Wall -Wextra -O0 -g -D_POSIX_C_SOURCE=200809L
LDFLAGS = -static
LDLIBS  =

all: assem.x disasm.x emu.x

assem.x: assem.o printe.o
disasm.x: disasm.o printe.o
emu.x: emu.o printe.o
	$(CC) -pg -o $@ $^ $(LDLIBS) `sdl2-config --libs`
emu.o: emu.c
	$(CC) $(CFLAGS) -pg `sdl2-config --cflags` -c -o $@ $<

assem: assem.x
	./assem.x 'pong.csm' 'pong2.rom'
clean:
	rm -rf *.o *.x
disasm: disasm.x
	./disasm.x 'pong.rom'
emu: emu.x
	./emu.x 'pong.rom'

.SUFFIXES: .c .o .x

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<
.o.x:
	$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS)
